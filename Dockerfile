FROM python:3.7@sha256:6008006c63b0a6043a11ac151cee572e0c8676b4ba3130ff23deff5f5d711237

WORKDIR /cert-viewer

COPY requirements.txt /cert-viewer/
RUN pip -V
RUN pip install -r /cert-viewer/requirements.txt
COPY conf_*.ini /cert-viewer/
COPY run.py /cert-viewer/
ADD . /cert-viewer
ADD cert_data /etc/cert_data

ENV CONFIG_FILE conf_local.ini
EXPOSE 5000
CMD ["python", "/cert-viewer/run.py"]
