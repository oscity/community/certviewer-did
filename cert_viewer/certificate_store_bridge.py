from flask import request
from cert_viewer import app

def award(certificate_uid):
    requested_format = request.args.get('format', None)
    if requested_format == 'json':
        return get_award_json(certificate_uid)
    from . import cert_store, certificate_formatter
    award, verification_info = certificate_formatter.get_formatted_award_and_verification_info(cert_store,
                                                                                               certificate_uid)
    return {'award': award,
            'verification_info': verification_info}


def get_award_json(certificate_uid):
    from . import cert_store
    certificate_json = cert_store.get_certificate_json(certificate_uid)
    return certificate_json

def get_certificates():
    from . import cert_store
    if hasattr(cert_store.__class__, 'get_certificates') and callable(getattr(cert_store.__class__, 'get_certificates')):
        certificates_list = cert_store.get_certificates()
    else: 
        certificates_list = app.config['RECENT_CERT_IDS']
    return [{"id": "", "text": ""}] + certificates_list

def get_revocations():
    from . import cert_store
    if hasattr(cert_store.__class__, 'get_revocations') and callable(getattr(cert_store.__class__, 'get_revocations')):
        revocations_list = cert_store.get_revocations()
    else: 
        revocations_list = []
    return revocations_list
