
import os
import os.path
import shutil
import json

from simplekv.fs import KeyValueStore, UrlMixin, CopyMixin, FilesystemStore
from simplekv._compat import url_quote, text_type
from threading import Thread
import uuid
from google.cloud import storage
from string import Template

from cert_viewer.firebase_connector import FirebaseConnector


class CloudStorageStore(KeyValueStore, UrlMixin, CopyMixin):
    """Store data in files on Cloud Storage

    The *CloudStorageStore* stores every value as its own file,
    all under a common directory in a bucket.

    Any call to :meth:`.url_for` will result in a `file://`-URL pointing
    towards the internal storage to be generated.
    """
    def __init__(self, bucket, directory, config, perm=None, **kwargs):
        """Initialize new FilesystemStore

        When files are created, they will receive permissions depending on the
        current umask if *perm* is `None`. Otherwise, permissions are set
        expliicitly.

        Note that when using :func:`put_file` with a filename, an attempt to
        move the file will be made. Permissions and ownership of the file will
        be preserved that way. If *perm* is set, permissions will be changed.

        :param root: the base directory for the store
        :param perm: the permissions for files in the filesystem store
        """
        super(CloudStorageStore, self).__init__(**kwargs)
        self.config = config
        self.bucket = bucket
        self.directory = directory
        self.perm = perm
        self.bufsize = 1024 * 1024  # 1m

        self.storage_client = storage.Client()

    def get_revocations(self):
        if (self.config.firebase_collections): 
            """
            Firebase certificates collection
            """
            firebase =  FirebaseConnector(self.config)
            return firebase.get_revocations()
        else:
            return []
    
    def list(self):
        if (self.config.firebase_collections): 
            """
            Firebase certificates collection
            """
            firebase =  FirebaseConnector(self.config)
            return firebase.get_certificates()
        else:
            """
            Cloud Storage connection
            """
            blobs = self.storage_client.list_blobs(
                self.config.cloud_storage_bucket, prefix=self.config.cloud_storage_dir)
            results = []
            threads = []
            template = Template(self.config.index_template)
            for blob in blobs:
                t = Thread(target=self.get_blob_info_thread, args=(results, blob, template))
                threads.append(t)
                t.start()
                
            for t in threads:
                t.join()
            return results

    def get_blob_info_thread(self, results, blob, template):
        uuid = blob.name.replace(self.config.cloud_storage_dir+'/', '').replace('.json', '')
        # Warning! Takes a lot of time to get all data, parse it to get template text 
        if (self.config.index_template):
            data = json.loads(blob.download_as_string()) 
            text = template.substitute(data)
        else: 
            text = uuid
        results.append({
            "text": text,
            "id": uuid
        })


    def get(self, filename): 
        """
        Custom: Get file from Cloud Storage connection
        """
        file = self.storage_client.bucket(self.bucket).blob(os.path.join(self.directory, filename))
        bcontent = file.download_as_string()
        return bcontent

    def _remove_empty_parents(self, path):
        parents = os.path.relpath(path, os.path.abspath(self.root))
        while len(parents) > 0:
            absparent = os.path.join(self.root, parents)
            if os.path.isdir(absparent):
                if len(os.listdir(absparent)) == 0:
                    os.rmdir(absparent)
                else:
                    break
            parents = os.path.dirname(parents)

    def _build_filename(self, key):
        return os.path.abspath(os.path.join(self.root, key))

    def _delete(self, key):
        try:
            targetname = self._build_filename(key)
            os.unlink(targetname)
            self._remove_empty_parents(targetname)
        except OSError as e:
            if not e.errno == 2:
                raise

    def _fix_permissions(self, filename):
        current_umask = os.umask(0)
        os.umask(current_umask)

        perm = self.perm
        if self.perm is None:
            perm = 0o666 & (0o777 ^ current_umask)

        os.chmod(filename, perm)

    def _has_key(self, key):
        return os.path.exists(self._build_filename(key))

    def _open(self, key):
        try:
            f = open(self._build_filename(key), 'rb')
            return f
        except IOError as e:
            if 2 == e.errno:
                raise KeyError(key)
            else:
                raise

    def _copy(self, source, dest):
        try:
            source_file_name = self._build_filename(source)
            dest_file_name = self._build_filename(dest)

            self._ensure_dir_exists(os.path.dirname(dest_file_name))
            shutil.copy(source_file_name, dest_file_name)
            self._fix_permissions(dest_file_name)
            return dest
        except IOError as e:
            if 2 == e.errno:
                raise KeyError(source)
            else:
                raise

    def _ensure_dir_exists(self, path):
        if not os.path.isdir(path):
            try:
                os.makedirs(path)
            except OSError as e:
                if not os.path.isdir(path):
                    raise e

    def _put_file(self, key, file):
        bufsize = self.bufsize

        target = self._build_filename(key)
        self._ensure_dir_exists(os.path.dirname(target))

        with open(target, 'wb') as f:
            while True:
                buf = file.read(bufsize)
                f.write(buf)
                if len(buf) < bufsize:
                    break

        # when using umask, correct permissions are automatically applied
        # only chmod is necessary
        if self.perm is not None:
            self._fix_permissions(target)

        return key

    def _put_filename(self, key, filename):
        target = self._build_filename(key)
        self._ensure_dir_exists(os.path.dirname(target))
        shutil.move(filename, target)

        # we do not know the permissions of the source file, rectify
        self._fix_permissions(target)
        return key

    def _url_for(self, key):
        full = os.path.abspath(self._build_filename(key))
        parts = full.split(os.sep)
        location = '/'.join(url_quote(p, safe='') for p in parts)
        return 'file://' + location

    def keys(self, prefix=u""):
        root = os.path.abspath(self.root)
        result = []
        for dp, dn, fn in os.walk(root):
            for f in fn:
                key = os.path.join(dp, f)[len(root) + 1:]
                if key.startswith(prefix):
                    result.append(key)
        return result

    def iter_keys(self, prefix=u""):
        return iter(self.keys(prefix))

    def iter_prefixes(self, delimiter, prefix=u""):
        if delimiter != os.sep:
            return super(FilesystemStore, self).iter_prefixes(
                delimiter,
                prefix,
            )
        return self._iter_prefixes_efficient(delimiter, prefix)

    def _iter_prefixes_efficient(self, delimiter, prefix=u""):
        if delimiter in prefix:
            pos = prefix.rfind(delimiter)
            search_prefix = prefix[:pos]
            path = os.path.join(self.root, search_prefix)
        else:
            search_prefix = None
            path = self.root

        try:
            for k in os.listdir(path):
                subpath = os.path.join(path, k)

                if search_prefix is not None:
                    k = os.path.join(search_prefix, k)

                if os.path.isdir(subpath):
                    k += delimiter

                if k.startswith(prefix):
                    yield k
        except OSError:
            # path does not exists
            pass

  