from cert_viewer import helpers
from cert_core import BlockchainType
import json


def certificate_to_award(displayable_certificate):
    tx_url = helpers.get_tx_lookup_chain(
        displayable_certificate.chain, displayable_certificate.txid)

    award = {
        'logoImg': displayable_certificate.issuer.image,
        'name': displayable_certificate.recipient_name,
        'title': displayable_certificate.title,
        'organization': displayable_certificate.issuer.name,
        'text': displayable_certificate.description,
        'issuerID': displayable_certificate.issuer.id,
        'chain': get_displayable_blockchain_type(displayable_certificate.chain.blockchain_type),
        'transactionID': displayable_certificate.txid,
        'transactionIDURL': tx_url,
        'issuedOn': displayable_certificate.issued_on.strftime('%Y-%m-%d'),
        'certificate_json': displayable_certificate.certificate_json
    }
    if displayable_certificate.signature_image:
        # TODO: format images and titles for all signers
        award['signatureImg'] = displayable_certificate.signature_image[0].image

    if displayable_certificate.subtitle:
        award['subtitle'] = displayable_certificate.subtitle
    
    try:
        if displayable_certificate.certificate_json['ganadores']:
            award['ganadores'] = json.loads(displayable_certificate.certificate_json['ganadores'])
    except:
        award['ganadores'] = None
    try:
        if displayable_certificate.certificate_json['premioespecial']:
            award['premioespecial'] = json.loads(displayable_certificate.certificate_json['premioespecial'])
    except:
        award['premioespecial'] = None
    try:
        if displayable_certificate.certificate_json['domicilios']:
            award['domicilios'] = json.loads(displayable_certificate.certificate_json['domicilios'])
    except:
        award['domicilios'] = None
    try:
        if displayable_certificate.certificate_json['actividades_secundarias']:
            award['actividades_secundarias'] = json.loads(displayable_certificate.certificate_json['actividades_secundarias'])
    except:
        award['actividades_secundarias'] = None
    try:
        if displayable_certificate.certificate_json['actividad_principal']:
            award['actividad_principal'] = json.loads(displayable_certificate.certificate_json['actividad_principal'])
    except:
        award['actividad_principal'] = None


    return award


def get_formatted_award_and_verification_info(cert_store, certificate_uid):
    """
    Propagates KeyError if not found
    :param certificate_uid:
    :return:
    """
    certificate_model = cert_store.get_certificate(certificate_uid)
    award = certificate_to_award(certificate_model)
    verification_info = {
        'uid': str(certificate_uid)
    }
    return award, verification_info


def get_displayable_blockchain_type(chain):
    if chain == BlockchainType.bitcoin:
        return 'Bitcoin'
    elif chain == BlockchainType.ethereum:
        return 'Ethereum'
    elif chain == BlockchainType.mock:
        return 'Mock'
    elif chain == BlockchainType.rsk:
        return 'Rsk'
    elif chain == BlockchainType.bfa:
        return 'Bfa'
    elif chain == BlockchainType.lacchain:
        return 'Lacchain'
    elif chain == BlockchainType.etc:
        return 'Etc'
    else:
        return None
