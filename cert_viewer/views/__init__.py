from flasgger import Swagger
from flask.views import View
import logging
import os
import sys
import json

from os import listdir
from os.path import isfile, join
from flask import jsonify, redirect
from flask_themes2 import render_theme_template, static_file_url
from werkzeug.routing import BaseConverter
from string import Template
from google.cloud import storage

from cert_viewer import certificate_store_bridge
from cert_viewer import introduction_store_bridge
from cert_viewer import verifier_bridge

DEFAULT_THEME = 'default'


def update_app_config(app, config):
    app.config.update(
        SECRET_KEY=config.secret_key,
        ISSUER_NAME=config.issuer_name,
        SITE_DESCRIPTION=config.site_description,
        ISSUER_LOGO_PATH=config.issuer_logo_path,
        ISSUER_EMAIL=config.issuer_email,
        THEME=config.theme,
        REDIRECT_MAIN_PAGE=config.redirect_main_page
    )
    recent_certs = update_recent_certs(app, config)
    app.config['RECENT_CERT_IDS'] = recent_certs


def update_recent_certs(app, config):
    """
    Local files
    """
    cert_path = config.cert_store_path
    certs_folder = []
    if config.index_template:
        template = Template(config.index_template)
    if os.path.exists(cert_path):
        for file in listdir(cert_path):
            if len(file) > 20 and file[len(file) - 4:] == "json":
                uuid = file[:len(file) - 5]
                # if config.index_template:
                #     # Replace text in template, takes a lot of time
                #     filename = os.path.abspath(os.path.join(cert_path, file))
                #     with open(filename) as f:
                #         data = json.load(f)
                #         text = template.safe_substitute(data)
                # else:
                text = uuid
                certs_folder.append({
                    "text": text,
                    "id": uuid
                })
    return certs_folder


def render(template, **context):
    from cert_viewer import app
    return render_theme_template(app.config['THEME'], template, **context)


def configure_views(app, config):
    update_app_config(app, config)
    add_rules(app, config)


class GenericView(View):
    def __init__(self, template):
        self.template = template

        super(GenericView, self).__init__()

    def dispatch_request(self):
        return render(self.template)


def add_rules(app, config):
    from cert_viewer.views.award_view import AwardView
    from cert_viewer.views.json_award_view import JsonAwardView
    from cert_viewer.views.renderable_view import RenderableView
    from cert_viewer.views.issuer_view import IssuerView
    from cert_viewer.views.verify_view import VerifyView
    from cert_viewer.views.request_view import RequestView

    update_app_config(app, config)
    app.url_map.converters['regex'] = RegexConverter

    if app.config['REDIRECT_MAIN_PAGE']:
        app.add_url_rule('/', view_func=redirect_to_nuxt)
    else:
        app.add_url_rule(
            '/', view_func=GenericView.as_view('index', template='index.html'))

    app.add_url_rule(rule='/<certificate_uid>', endpoint='award',
                     view_func=AwardView.as_view(name='award', template='award.html',
                                                 view=certificate_store_bridge.award))

    app.add_url_rule('/certificates',
                     view_func=JsonAwardView.as_view('certificates', view=certificate_store_bridge.get_certificates))

    app.add_url_rule('/certificate/<certificate_uid>',
                     view_func=JsonAwardView.as_view('certificate', view=certificate_store_bridge.get_award_json))

    app.add_url_rule('/verify/<lenguage>/<certificate_uid>',
                     view_func=VerifyView.as_view('verify', view=verifier_bridge.verify))

    app.add_url_rule(
        '/intro/', view_func=introduction_store_bridge.insert_introduction, methods=['POST', ])
    app.add_url_rule('/request', view_func=RequestView.as_view(name='request'))
    app.add_url_rule(
        '/faq', view_func=GenericView.as_view('faq', template='faq.html'))
    app.add_url_rule(
        '/bitcoinkeys', view_func=GenericView.as_view('bitcoinkeys', template='bitcoinkeys.html'))

    app.add_url_rule('/issuer/<issuer_file>', view_func=issuer_page)

    # Redirect issuer files to the right theme, generate revocation file
    app.add_url_rule('/_themes/<theme>/issuer/<issuer_file>',
                     view_func=redirect_issuer_files)

    app.add_url_rule('/spec', view_func=spec)

    app.register_error_handler(404, page_not_found)
    app.register_error_handler(KeyError, key_error)
    app.register_error_handler(500, internal_server_error)
    app.register_error_handler(Exception, unhandled_exception)


def redirect_to_nuxt():
    from cert_viewer import app
    return redirect(app.config['REDIRECT_MAIN_PAGE'], code=302)


def spec():
    from cert_viewer import app

    return jsonify(Swagger(app))


def issuer_page(issuer_file):
    from cert_viewer import app
    the_url = static_file_url(theme=app.config['THEME'], filename=(
        os.path.join('issuer/', issuer_file)))
    return redirect(the_url, code=302)


def redirect_issuer_files(theme, issuer_file):
    """
    Redirect to use issuer files only from the SELECTED THEME
    Eg: /_themes/notexist/issuer/issuer.json -> /_themes/selectedtheme/issuer/issuer.json
    """
    from cert_viewer import app
    the_url = static_file_url(theme=app.config['THEME'], filename=(
        os.path.join('issuer/',  issuer_file)))
    if theme != app.config['THEME']:
        # Theme is not the selected one, redirect
        return redirect(the_url, code=302)
    else:
        # Theme is right, return JSON file
        file_path = os.path.join(os.getcwd(), the_url.replace(
            '/_', 'cert_viewer/').replace('issuer/', 'static/issuer/'))
        if os.path.exists(file_path):
            with open(file_path) as file:
                json_file = json.load(file)

                if (issuer_file == "revocation.json" or issuer_file == "revocation-upso.json"):
                    # Dirty fix for upso because its not homologated
                    """
                    Generate revocation file 
                    """
                    revocations = certificate_store_bridge.get_revocations()
                    # Concat default revokedAssertions with database results
                    json_file["revokedAssertions"] += revocations
                    # Remove duplicates
                    json_file["revokedAssertions"] = [dict(t) for t in {tuple(
                        d.items()) for d in json_file["revokedAssertions"]}]
                return jsonify(json_file)
        else:
            return 'File not found', 404


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


# Errors
def page_not_found(error):
    logging.error('Page not found: %s', error, exc_info=True)
    return 'This page does not exist', 404


def key_error(error):
    key = error.args[0]
    logging.error('Error: %s', error, exc_info=True)
    logging.error('Key not found not found: %s, error: ', key)

    message = 'Key not found: ' + key
    return message, 404


def internal_server_error(error):
    logging.error('Server Error: %s', error, exc_info=True)
    return 'Server error: {0}'.format(error), 500


def unhandled_exception(e):
    logging.exception('Unhandled Exception: %s', e, exc_info=True)
    return 'Unhandled exception: {0}'.format(e), 500
