from google.cloud import firestore
from string import Template
import re

class FirebaseConnector():
    def __init__(self, config):
        super().__init__()
        self.config = config
        self.db = firestore.Client()

    def get_certificates(self):
        results = []
        template = Template(self.config.index_template)
        for firebase_collection in self.config.firebase_collections:
            docs = self.db.collection(firebase_collection).where('estado_origen', '==', "Certificado").stream()
            # Display document documents
            for doc in docs:
                document = doc.to_dict()
                # Document link includes uuid reference. Eg: https://rionegro.os.city/e540313b-57c9-4542-a95f-8c8bb301caf4
                uuid = document['link'].split('/').pop()
                results.append({
                    "text": re.sub(r'(^|-\W)\$\w+', '', template.safe_substitute(document)), # Use safe_substitute if field is not included, remove $template_vars after that
                    "id": uuid
                })
        return sorted(results, key = lambda d: d['text']) # Sort by text ascending

    def get_revocations(self):
        results = []
        for firebase_collection in self.config.firebase_collections:
            docs = self.db.collection(firebase_collection).where('estado_origen', '==', "Revocado").stream()
            # Display document documents
            for doc in docs:
                document = doc.to_dict()
                # Document link includes uuid reference. Eg: https://rionegro.os.city/e540313b-57c9-4542-a95f-8c8bb301caf4
                uuid = document['link'].split('/').pop()
                revocation = {
                    "id": "urn:uuid:" + uuid,
                    "revocationReason": 'Example revocation, add as needed'
                }
                if "revocationReason" in document:
                    revocation["revocationReason"] = document['revocationReason']
                results.append(revocation)
        return sorted(results, key = lambda d: d['id']) # Sort by text ascending
