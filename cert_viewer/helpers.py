import binascii
import sys

from cert_core import Chain, UnknownChainError

unhexlify = binascii.unhexlify
hexlify = binascii.hexlify
if sys.version > '3':
    def unhexlify(h): return binascii.unhexlify(h.encode('utf8'))
    def hexlify(b): return binascii.hexlify(b).decode('utf8')


def obfuscate_email_display(email):
    """Partially hides email before displaying"""
    hidden_email_parts = email.split("@")
    hidden_email = hidden_email_parts[0][:2] + (
        "*" * (len(hidden_email_parts[0]) - 2)) + "@" + hidden_email_parts[1]
    return hidden_email


def get_tx_lookup_chain(chain, txid):
    if chain == Chain.bitcoin_testnet:
        return 'https://live.blockcypher.com/btc-testnet/tx/' + txid
    elif chain == Chain.bitcoin_mainnet:
        return 'https://blockchain.info/tx/' + txid
    elif chain == Chain.bitcoin_regtest or chain == Chain.mockchain:
        return 'This has not been issued on a blockchain and is for testing only'
    elif chain == Chain.ethereum_mainnet:
        return 'https://etherscan.io/tx/' + txid
    elif chain == Chain.ethereum_ropsten:
        return 'https://ropsten.etherscan.io/tx/' + txid
    elif chain == Chain.rsk_mainnet:
        return 'https://explorer.rsk.co/tx/' + txid
    elif chain == Chain.rsk_testnet:
        return 'https://explorer.testnet.rsk.co/tx/' + txid
    elif chain == Chain.bfa_mainnet:
        return 'http://bfascan.com.ar/tx/' + txid
    elif chain == Chain.bfa_testnet:
        return 'http://bfascan.com.ar/tx/' + txid
    elif chain == Chain.lacchain_mainnet:
        return 'https://explorer.lacchain.net/tx/' + txid
    elif chain == Chain.lacchain_testnet:
        return 'http://34.123.38.34/explorer/tx/' + txid
    elif chain == Chain.etc_mainnet:
        return 'https://gastracker.io/tx/' + txid
    elif chain == Chain.etc_testnet:
        return 'https://kottiexplorer.ethernode.io/tx/' + txid

    else:
        raise UnknownChainError(
            'unsupported chain (%s) requested with blockcypher collector. Currently only testnet and mainnet are supported' % chain)
