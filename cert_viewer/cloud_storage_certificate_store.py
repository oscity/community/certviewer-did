"""Retrieves certificates from mongodb."""
import logging

from cert_core.cert_store import helpers
from cert_core.cert_model import model
from cert_core import URN_UUID_PREFIX


def certificate_uid_to_filename(uid):
    return uid + '.json'


class CloudStorageCertificateStore:
    def __init__(self, kv_store=None):
        """Create a CloudStorageCertificateStore
        :param kv_store: key value store; subclass of KeyValueStore
        """
        self.kv_store = kv_store

    def get_certificates(self):
        """
        Returns certificates as list.
        :return:
        """
        certificates_list = self.kv_store.list()
        return certificates_list

    def get_revocations(self):
        """
        Returns revocations as list.
        :return:
        """
        revocations_list = self.kv_store.get_revocations()
        return revocations_list

    def get_certificate(self, certificate_uid):
        """
        Returns a certificate. Propagates KeyError if key isn't found
        :param certificate_uid:
        :return:
        """
        certificate_json = self.get_certificate_json(certificate_uid)
        return model.to_certificate_model(certificate_json)

    def get_certificate_json(self, certificate_uid):
        """
        Returns certificate as json. Propagates KeyError if key isn't found
        :param certificate_uid:
        :return:
        """

        if certificate_uid.startswith(URN_UUID_PREFIX):
            uid = certificate_uid[len(URN_UUID_PREFIX):]
        elif certificate_uid.startswith('http'):
            last_slash = certificate_uid.rindex('/')
            uid = certificate_uid[last_slash + 1:]
        else:
            uid = certificate_uid
        logging.debug('Retrieving certificate for uid=%s', uid)
        certificate_bytes = self._get_certificate_raw(uid)
        logging.debug('Found certificate for uid=%s', uid)
        certificate_json = helpers.certificate_bytes_to_json(certificate_bytes)
        return certificate_json

    def _get_certificate_raw(self, certificate_uid):
        """
        Returns certificate as raw bytes. Per kvstore contract, raises an KeyError if key isn't found.
        :param certificate_uid:
        :return:
        """
        cert_file_bytes = self.kv_store.get(certificate_uid_to_filename(certificate_uid))
        return cert_file_bytes
