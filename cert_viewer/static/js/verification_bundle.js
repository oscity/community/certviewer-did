(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.verification = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
function hidefields(fields) {
    for (i = 0; i < fields.length; i++) {
        $(fields[i]).hide();
    }
}

function renderResponse(i, len, data) {
    setTimeout(function () {
        count = i + 1;
        total = len - 1;
        message = data.name;
        value = data.status;
        if (i != len - 1) {
            $("#progress-msg").html($("#progress-msg").html() + 'Step ' + count.toString() + ' of ' + total.toString() + "... " + message + '</span>');
        }
        setTimeout(function () {
            if (i == len - 1) {
                if (value == "passed" || value == "done") {
                    $("#progress-msg").html($("#progress-msg").html() + "Success! The certificate has been verified.")
                    $("#verified").show();
                }
                else {
                    $("#progress-msg").html($("#progress-msg").html() + "Oops! The certificate could not be verified.")
                    $("#not-verified").show();
                }
            }
            else {
                $("#progress-msg").html($("#progress-msg").html() + ' [' + markMappings[value] + ']<br>')
            }
        }, 1000)

    }, timeDelay * i);
}

timeDelay = 2000;
markMappings = {"passed": "PASS", "failed": "FAIL", "done": "DONE", "not_started": "NOT STARTED"}

$(document).ready(function () {
    $("#verify-button").click(function () {
        hidefields(["#not-verified", "#verified"]);
        $("#progress-msg").html("");
        var data = $(this).attr('value');
        var uid = JSON.parse(data.replace(/'/g, '"')).uid;
        $.get("/verify/" + uid, function (res) {
            res = JSON.parse(res);
            $("#progress-msg").show();
            if (res == null) {
                $("#progress-msg").html($("#progress-msg").html() + "Oops! There was an issue connecting to the Blockchain.info API")
            }
            else {
                for (i = 0; i < res.length; i++) {
                    renderResponse(i, res.length, res[i])
                }
            }
        });
    });
});
},{}]},{},[1])(1)
});
