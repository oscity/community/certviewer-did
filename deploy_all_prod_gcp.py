from string import Template
import os
import sys
import json
from dotenv import load_dotenv
import subprocess
# import logging
from loguru import logger

# logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)
load_dotenv()

with open('deploy_configs.json') as json_file:
    configs = json.load(json_file)

    # subprocess.run("docker build -t cert-viewer .", shell=True, check=True)

    if len(sys.argv) > 1:
        # Use first argument to deploy only selected config
        selected_conf = sys.argv[1]
        selected_configs = filter(
            lambda conf: conf['CONFIG_FILE'] == selected_conf, configs)
    else:
        # Deploy all configs
        selected_configs = configs

    for config in selected_configs:
        key_file = os.getenv(config['KEY_FILE'])
        config_gcloud = Template(
            "gcloud auth activate-service-account --key-file $KEY_FILE && gcloud config set project $PROJECT_ID").substitute(config, KEY_FILE=key_file)
        logger.info(config_gcloud)
        subprocess.run(config_gcloud, shell=True, check=True)

        # docker tag cert-viewer gcr.io/$PROJECT_ID/cert-viewer && docker push gcr.io/$PROJECT_ID/cert-viewer
        # gcloud builds submit --tag gcr.io/$PROJECT_ID/cert-viewer
        gcloud_submit = Template(
            "gcloud builds submit --tag gcr.io/$PROJECT_ID/cert-viewer").substitute(config)
        logger.info(gcloud_submit)
        subprocess.run(gcloud_submit, shell=True, check=True)

        # Deploy in Cloud Run
        gcloud_run = Template(
            "gcloud run deploy $APP_NAME --image gcr.io/$PROJECT_ID/cert-viewer --platform managed --port 5000 --set-env-vars CONFIG_FILE=$CONFIG_FILE").substitute(config)
        logger.info(gcloud_run)

        if 'API_KEY' in config:
            api_key = os.getenv(config['API_KEY'])
            gcloud_run += f",ETHERSCAN_API_TOKEN={api_key}"
            logger.info(f"API_KEY : {config['API_KEY']}")
        gcloud_run += ' --region us-east1 --allow-unauthenticated'

        subprocess.run(gcloud_run, shell=True, check=True)
